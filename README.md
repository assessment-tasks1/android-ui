# Android UI Assessment Task

## Build Android app using following screenshots 
(Note: All required assets and images are provided in specific folder of these screenshots )

## Features

- Add Google Auth
- Store All data and images to firebase  
- Add animations and transitions 

## Screenshots

<p align="left">
  <img width=150 title="Splash Screen" src="/01%20Splas%20Screen/01%20Splas%20Screen.png">
  <img width=150 title="Connect With Google" src="/02%20Connect%20with%20google/02%20Contact%20with%20google.png">
  <img width=150 title="Registration Step 1" src="/03%20Create%20Account/03%20Create%20Account.png">
  <img width=150 title="Registration Step 2" src="/04%20Create%20Account_2/04%20Create%20Account_2.png">
  <img width=150 title="Privacy" src="/05%20Account%20Privacy/05%20Account%20Privacy.png">
  <img width=150 title="Successful" src="/06%20Successfully/06%20Successfully.png">
</p>

